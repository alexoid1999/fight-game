import {showModal} from './modal';
import {createFighterImage} from '../fighterPreview';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';
import { createFighters } from '../fightersView';
import { fighterService } from '../../services/fightersService';

export function showWinnerModal(fighter) {
  // call showModal function 
  let bodyElement = createElement({tagName:'div', className:'modal-body'});

  bodyElement.appendChild(createFighterImage(fighter));

  let onClose = async()=>{
    // Создаём новый экземпляр селектора бойцов и удаляем прошлую арену
    const fighters = await fighterService.getFighters();
    const fightersElement = createFighters(fighters);
    App.rootElement.removeChild(document.getElementsByClassName('arena___root')[0]);
    App.rootElement.appendChild(fightersElement);
  }

  return showModal({title:`The winner is ${fighter.name}`, bodyElement, onClose})
}
